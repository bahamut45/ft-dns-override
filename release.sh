#!/usr/bin/env bash

DIST_DIR="dist"
BUILD_DIR="build"
TAG_CMD="git describe --tags --abbrev=0"
BUMP_TYPE=${1:-'patch'}

echo "Removing contents of $DIST_DIR and $BUILD_DIR directories"
rm -rf ${DIST_DIR:?}/* ${BUILD_DIR:?}/*

# Check the provided argument
case "$BUMP_TYPE" in
  'major')
    echo "Bumping major version"
    bump-my-version bump major
    ;;
  'minor')
    echo "Bumping minor version"
    bump-my-version bump minor
    ;;
  'patch')
    echo "Bumping patch version"
    bump-my-version bump patch
    ;;
  *) # default case
    echo "$BUMP_TYPE is not a valid argument. Please use either 'major', 'minor' or 'patch' as argument."
    exit 1
    ;;
esac

echo "Pushing to master branch with tags"
git push --progress --porcelain origin refs/heads/master:master --tags

echo "Building with briefcase"
briefcase build -u -r --target ubuntu:noble
briefcase build -u -r --target ubuntu:jammy

echo "Packing with briefcase"
briefcase package -u --target ubuntu:noble
briefcase package -u --target ubuntu:jammy

echo "Generating the tag with the command: $TAG_CMD"
TAG=$($TAG_CMD)
echo "Tag: ${TAG}"

echo "Creating release with glab for tag: ${TAG}"
glab release create "${TAG}"

echo "Uploading release to glab"
for pkg in dist/*;
do
  pkg_formatted=${pkg/'~'/_}
  mv "${pkg}" "${pkg_formatted}"
  glab release upload "${TAG}" "${pkg_formatted}"
done
