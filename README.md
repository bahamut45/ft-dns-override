# FORTICLIENT DNS OVERRIDE

Permet de surcharger les options dns des vpns fortinet.

## Version pris en charge
- forticlient  7.0.7.0246

## Pré-requis

1. Désactiver la gestion du dns de NetworkManager

   - Ajouter la ligne `dns=default`
   - Dans le fichier `/etc/NetworkManager/NetworkManager.conf`

    ```ini
    [main]
    plugins=ifupdown,keyfile
    dns=default
    
    [ifupdown]
    managed=false
    
    [device]
    wifi.scan-rand-mac-address=no
    ```
   
2. Désactiver la partir stub du `resolv.conf` 
   ```bash
   rm -f /etc/resolv.conf
   ln -sv /run/systemd/resolve/resolv.conf /etc/resolv.conf
   ```

3. Autoriser la commande `resolvectl` sans mot de passe root
    ```bash
    echo "$USER ALL=NOPASSWD:/usr/bin/resolvectl" |  sudo tee /etc/sudoers.d/resolvectl-$USER
    ```

