"""
Permet de surcharger les options dns des vpns fortinet.
"""
import sys

from ft_dns_override.systray import ForticlientDNSOverrideSystray

try:
    from importlib import metadata as importlib_metadata
except ImportError:
    # Backwards compatibility - importlib.metadata was added in Python 3.8
    # noinspection PyUnresolvedReferences
    import importlib_metadata

from PySide6 import QtWidgets


def main():
    # Linux desktop environments use app's .desktop file to integrate the app
    # to their application menus. The .desktop file of this app will include
    # StartupWMClass key, set to app's formal name, which helps associate
    # app's windows to its menu item.
    #
    # For association to work any windows of the app must have WMCLASS
    # property set to match the value set in app's desktop file. For PySide2
    # this is set with setApplicationName().

    # Find the name of the module that was used to start the app
    app_module = sys.modules['__main__'].__package__
    # Retrieve the app's metadata
    metadata = importlib_metadata.metadata(app_module)

    QtWidgets.QApplication.setApplicationName(metadata['Formal-Name'])

    app = QtWidgets.QApplication(sys.argv)
    app.setQuitOnLastWindowClosed(False)

    system_tray_app = ForticlientDNSOverrideSystray(app, metadata)
    system_tray_app.start()
    sys.exit(app.exec())
