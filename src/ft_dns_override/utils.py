import logging
import sys
from logging.handlers import TimedRotatingFileHandler
from pathlib import Path
from typing import Union, Optional, List, Dict, Any

from PySide6 import QtGui
from PySide6.QtCore import QSettings, QStandardPaths
from dbus import Interface, SessionBus

INTERVALS = [5, 10, 15, 30, 60, 120, 180, 300]
FORTICLIENT_LOG = '/var/log/forticlient/sslvpn.log'
FORTIVPN_LOCATION = '/opt/forticlient/fortivpn'
LOG_FILE = 'ft-dns-override.log'
LOG_FMT = ('%(asctime)s - %(name)s:%(lineno)d(%(funcName)s) '
           '%(levelname)s: %(message)s')
formatter = logging.Formatter(LOG_FMT)

app_images_path = Path(__file__).resolve().parent

app_icon_path = app_images_path / 'resources'

app_settings = QSettings('ft-dns-override', 'settings')
settings_data_dir = Path('ft-dns-override')

app_settings_data_dir = QStandardPaths.writableLocation(QStandardPaths.AppConfigLocation) / settings_data_dir
app_settings_data_dir.mkdir(parents=True, exist_ok=True)

LOG_FILE_PATH = app_settings_data_dir / LOG_FILE

handler = TimedRotatingFileHandler(
    filename=str(LOG_FILE_PATH),
    when='midnight',
    backupCount=5,
    delay=False
)

handler.setFormatter(formatter)
handler.setLevel(logging.DEBUG)

logger = logging.getLogger('ft-dns-override')
logger.addHandler(handler)
logger.setLevel(logging.DEBUG)


def qt_icon(name: str, icon_path: Union[str, Path] = app_icon_path) -> QtGui.QIcon:
    """
    Creates instances of QtGui.QIcon representing the requested icon.

    Args:
        name (str): The name of the icon file.
        icon_path (Union[str, Path], optional): The path to the directory containing the icon file.
            Default value is the path to the application's icon file.

    Returns:
        QtGui.QIcon: An instance representing the requested icon.

    Raises:
        FileNotFoundError: If the specified icon file does not exist.
    """
    icon_path = Path(icon_path)
    icon_file = icon_path / name
    if not icon_file.is_file():
        raise FileNotFoundError(f'{icon_file} does not exist')
    return QtGui.QIcon(str(icon_file))


def send_notification(title: str, body: str, app_name: str = "Forticlient DNS Override",
                      app_icon: str = str(app_icon_path / "ft-dns-override-icon.png"),
                      timeout: int = 5000, actions: Optional[List[str]] = None,
                      hints: Optional[Dict[str, Any]] = None, replaces_id: int = 0):
    """
    Send a desktop notification.

    Args:
        title (str): The title of the notification.
        body (str): The body text of the notification.
        app_name (str, optional): The name of the app sending the notification. Defaults to "Forticlient DNS Override".
        app_icon (str, optional): The path to the icon of the app sending the notification.
            Defaults to str(app_icon_path / "ft-dns-override-icon.png").
        timeout (int, optional): The time before the notification automatically dismisses. Defaults to 5000.
        actions (List[str], optional): Actions that can be taken from the notification. Defaults to None.
        hints (Dict[str, Any], optional): Additional parameters for the notification. Defaults to None.
        replaces_id (int, optional): The id of the notification this notification replaces. Defaults to 0.

    Returns:
        None
    """
    if hints is None:
        hints = {}
    if actions is None:
        actions = []
    bus_name = "org.freedesktop.Notifications"
    object_path = "/org/freedesktop/Notifications"
    interface_name = bus_name

    bus = SessionBus()
    notifier = Interface(bus.get_object(bus_name, object_path), interface_name)

    notifier.Notify(app_name, replaces_id, app_icon, title, body, actions, hints, timeout)
