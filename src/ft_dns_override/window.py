from importlib.metadata import PackageMetadata
from typing import Any

from PySide6.QtCore import QSize, Qt
from PySide6.QtWidgets import QStyle, QMainWindow, QVBoxLayout, QWidget, QTabWidget, QLabel, QApplication

from ft_dns_override.logs import LogsDialog
from ft_dns_override.settings import SettingsDialog
from ft_dns_override.utils import qt_icon, logger


class ForticlientDNSOverride(QMainWindow):
    def __init__(self, app: QApplication, metadata: PackageMetadata, settings: Any):
        """
        Initializer/Constructor for the ForticlientDNSOverride class.

        Args:
            app (QApplication): The application.
            metadata (PackageMetadata): Package metadata.
            settings (Optional[Any]): Settings for the application.
        """
        super().__init__()
        self.status_bar = None
        self.status_bar_version = QLabel("Forticlient Version: 0")
        self.status_bar_state = QLabel("Forticlient State: disconnected")
        self._app = app
        self._metadata = metadata
        self._settings = settings
        self.init_ui()

    def init_ui(self) -> None:
        """
        Initializes the user interface.
        """
        self.setWindowTitle(self._metadata['Formal-Name'])
        self.setWindowIcon(qt_icon("ft-dns-override-icon.png"))
        self.status_bar = self.statusBar()
        self.status_bar.addPermanentWidget(self.status_bar_version)
        self.status_bar.addPermanentWidget(self.status_bar_state)
        self._init_window_content_and_aspect()

    def show(self) -> None:
        """
        Displays the window.
        """
        logger.debug('Show window application')
        super().show()

    def _init_window_content_and_aspect(self) -> None:
        """
        Initializes window content and aspect.
        """
        window_layout = QVBoxLayout()
        main_tab_widget = QTabWidget()

        settings_widget = SettingsDialog(self._settings, parent=self)
        self.logs_widget = LogsDialog(parent=self)

        main_tab_widget.addTab(settings_widget, 'Settings')
        main_tab_widget.addTab(self.logs_widget, 'Current Logs')

        window_layout.addWidget(main_tab_widget)

        widget = QWidget()
        widget.setLayout(window_layout)
        self.setCentralWidget(widget)

        screen_geom = self._app.screenAt(self.pos()).availableGeometry()
        window_size = QSize(screen_geom.width() * 0.4, screen_geom.height() * 0.1)
        self.setGeometry(
            QStyle.alignedRect(
                Qt.LeftToRight, Qt.AlignCenter, window_size, screen_geom
            )
        )
