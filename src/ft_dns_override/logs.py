import logging
from typing import Optional

from PySide6.QtWidgets import QApplication, QDialog, QTextEdit, QVBoxLayout, QPlainTextEdit, QWidget, QLabel

from ft_dns_override.utils import LOG_FILE_PATH


class QPlainTextEditLogger(logging.Handler):
    """
    A custom logging handler that sends log strings to a QPlainTextEdit.

    Args:
        parent (Optional[QPlainTextEdit]): Parent QPlainTextEdit receiving log messages. Defaults to None.

    """
    def __init__(self, parent: QTextEdit = None) -> None:
        super().__init__()
        self.widget = parent if parent is not None else QTextEdit()
        self.widget.setReadOnly(True)

    def emit(self, record: logging.LogRecord) -> None:
        """
        Emit a log record.

        If the stream is not None, the record is formatted, and if the
        filter does not reject the record, the record is written to the
        stream.

        Note::
            If "stream" is not none when the handler is created, a
            StreamHandler is created and is used to call the write() method.
            A TypeError will be raised if the stream does not have write()
            and flush() methods implemented.

        Normally, subclasses of this method will only need to implement
        emit() and possibly filter().

        The record is then passed to the handle() method. If the record was
        not handled, handle() will call this method.

        Args:
            record (LogRecord): Log record to be emitted.

        Raises:
            NotImplementedError: Needs to be implemented in subclasses.
        """
        msg = self.format(record)
        self.widget.append(msg)
        QApplication.processEvents()


class LogsDialog(QDialog):
    def __init__(self, parent: Optional[QWidget] = None) -> None:
        """
        Initialization method for the LogsDialog class.

        Args:
            parent : Optional[QWidget]: A reference to a parent widget, if any.

        """
        super().__init__(parent)
        self.log_text = None
        self.init_ui()

    def init_ui(self) -> None:
        """
        User interface initialization method for the LogsDialog class.

        """
        self.log_text = QTextEdit()
        self.log_text.setReadOnly(True)
        self.log_text.setAcceptRichText(True)
        label = QLabel(f'Full log is {LOG_FILE_PATH}')
        layout = QVBoxLayout()
        layout.addWidget(label)
        layout.addWidget(self.log_text)
        self.setLayout(layout)
