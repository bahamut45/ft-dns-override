import logging
import sys
from importlib.metadata import PackageMetadata
from typing import NoReturn

from PySide6.QtCore import QTimer
from PySide6.QtGui import QAction
from PySide6.QtWidgets import QApplication, QMenu, QSystemTrayIcon, QMessageBox, QWidget
from subprocrunner import SubprocessRunner

from ft_dns_override.forticlient import Forticlient
from ft_dns_override.logs import QPlainTextEditLogger
from ft_dns_override.settings import SettingsManager
from ft_dns_override.utils import qt_icon, logger, FORTIVPN_LOCATION
from ft_dns_override.window import ForticlientDNSOverride


class ForticlientDNSOverrideSystray:
    def __init__(self, app: QApplication, metadata: PackageMetadata) -> NoReturn:
        """
        Initializes the ForticlientDNSOverrideSystray.

        Args:
            app (QApplication): The QApplication instance.
            metadata (PackageMetadata): Metadata about the package.
        """
        self._app = app
        self._metadata = metadata
        self.settings = SettingsManager()

        self._open_action = self._action_show_main_window()
        self._quit_action = self._action_quit_app()
        self._menu = self._create_menu()
        self._tray_icon = self._create_tray_icon()
        self._timer = self._create_timer()

        self._main_window = ForticlientDNSOverride(self._app, self._metadata, self.settings)
        self.forticlient_version = self._check_forticlient_version()
        self.forticlient = Forticlient(self._tray_icon)
        self._add_handler()
        self._timer.start()

    def _create_menu(self) -> QMenu:
        """
        Creates a menu with open and quit actions.

        Returns:
            QMenu: The created menu.
        """
        menu = QMenu()
        menu.addAction(self._open_action)
        menu.addSeparator()
        menu.addAction(self._quit_action)
        return menu

    def _action_show_main_window(self) -> QAction:
        """
        Creates a QAction that opens the main window.

        Returns:
           QAction: The created action.
        """
        action = QAction('Open')
        action.triggered.connect(self._open_window)
        return action

    def _open_window(self) -> NoReturn:
        """
        Opens the main window.
        """
        self._main_window.show()

    def _action_quit_app(self) -> QAction:
        """
        Creates a QAction object with the label 'Quit' and connect it to the _quit_app method.

        Returns:
            QAction: The created action.
        """
        action = QAction('Quit')
        action.triggered.connect(self._quit_app)
        return action

    def _quit_app(self) -> NoReturn:
        """
        Quits the application by calling the `quit()` method of the `_app` object.
        """
        logger.debug('Close application')
        self._app.quit()

    def _create_tray_icon(self) -> QSystemTrayIcon:
        """
        Creates a system tray icon with the given parameters.

        Returns:
            QSystemTrayIcon: The created tray icon.
        """
        icon = qt_icon('ft-dns-override-icon.png')
        tray_icon = QSystemTrayIcon()
        tray_icon.setContextMenu(self._menu)
        tray_icon.setIcon(icon)
        tray_icon.setToolTip(self._metadata['Formal-Name'])
        return tray_icon

    def start(self) -> NoReturn:
        """
        Starts the application by performing necessary operations such as checking and displaying a message,
        then shows the tray icon.
        """
        logger.debug('Start application: %s', self._metadata['Formal-Name'])
        self._check_and_show_message()
        self._tray_icon.show()

    def _check_forticlient_version(self) -> str:
        """
        Checks the installed version of Forticlient. If the Forticlient is not installed,
        it displays an error message and exits the program.

        Returns:
            str: The installed version of Forticlient.
        """
        runner = SubprocessRunner(
            r"apt-cache policy forticlient| grep -Pho '\s+Installed: \K[0-9.]+'"
        )
        runner.run(env={"LANG": "C"})
        if runner.stdout:
            text = f'{runner.stdout.strip()}'
        else:
            QMessageBox.critical(
                QWidget(),
                self._metadata['Formal-Name'],
                'Forticlient not installed, please install before use.'
            )
            sys.exit(1)
        self._main_window.status_bar_version.setText(f'Forticlient Version: {text}')
        return text

    @staticmethod
    def _check_sudo_resolvectl() -> bool:
        """
        Check if the current user has sudo permissions to run the 'resolvectl' command.

        Returns:
            bool: True if the user has sudo permissions to run 'resolvectl', False otherwise.
        """
        runner = SubprocessRunner(
            "sudo -l | grep -q resolvectl"
        )
        runner.run()
        if runner.returncode != 0:
            return False
        else:
            return True

    @staticmethod
    def _check_network_manager_service() -> bool:
        """
        Check the status of the Network Manager service.

        This method uses the 'systemctl' command to check if the Network Manager service is active.

        Returns:
            bool: True if the Network Manager service is active, False otherwise.
        """
        runner = SubprocessRunner(
            "systemctl is-active --quiet NetworkManager.service"
        )
        runner.run()
        if runner.returncode != 0:
            return False
        else:
            return True

    @staticmethod
    def _check_dns_none_in_network_manager() -> bool:
        """
        Check if DNS is set to 'none' in NetworkManager configuration.

        Returns:
            bool: True if DNS is set to 'none', False otherwise.
        """
        runner = SubprocessRunner(
            "grep -q -E 'dns=none|dns=default' /etc/NetworkManager/NetworkManager.conf"
        )
        runner.run()
        if runner.returncode != 0:
            return False
        else:
            return True

    def _check_and_show_message(self) -> None:
        """
        This method checks various conditions and displays error messages
        using QMessageBox.critical if any of the conditions fail.

        Returns:
            None

        Usage Example:
            self._check_and_show_message()
        """
        checks = [
            ('sudo resolvectl not available', self._check_sudo_resolvectl),
            ('Network Manager service not active', self._check_network_manager_service),
            ('DNS attribute not found in Network Manager conf file', self._check_dns_none_in_network_manager),
        ]

        errors = []
        for message, check in checks:
            if not check():
                errors.append(message)
                QMessageBox.critical(
                    QWidget(),
                    self._metadata['Formal-Name'],
                    f'Check failed: {message}'
                )

        if errors:
            QMessageBox.critical(
                QWidget(),
                self._metadata['Formal-Name'],
                'Errors please read README.md, before next execution'
            )

        return None

    def _add_handler(self) -> None:
        """
        Adds a handler to the logger.

        This method configures a Qt-based handler for the logger and attaches it.
        The handler is configured with a specific format for log messages and a
        log level of DEBUG. The handler is added to the main logger.
        """
        # noinspection PyUnresolvedReferences
        qt_handler = QPlainTextEditLogger(self._main_window.logs_widget.log_text)
        qt_handler.setFormatter(logging.Formatter('%(asctime)s - %(levelname)s - %(message)s'))
        qt_handler.setLevel(logging.DEBUG)
        logger.addHandler(qt_handler)

    def _create_timer(self) -> QTimer:
        """
        Method to create a QTimer object.

        This method initializes a QTimer object with the application as the parent.
        The timer is configured to be non-single-shot and is set to trigger every interval specified
        in the application's settings. The timer's timeout signal is connected to the
        `_check_forticlient_status` method.

        Returns:
            QTimer: The created QTimer object.
        """
        timer = QTimer(self._app)
        timer.setSingleShot(False)
        timer.setInterval(self.settings.default_interval)
        # noinspection PyUnresolvedReferences
        timer.timeout.connect(self._check_forticlient_status)
        return timer

    def _check_forticlient_status(self) -> None:
        """
        This method checks the status of Forticlient. If the monitor mode in settings is enabled,
        it will check the status of the FortiVPN service. If the service is connected, it will
        set the Forticlient state as "Connected" and update the status bar of the main window.
        If the service is not connected, it will set the state as "Disconnected" and update the
        status bar accordingly. It will then check the DNS configuration. If monitor mode is not enabled,
        it will log that the monitoring mode is disabled.

        Returns:
            None
        """
        if self.settings.monitor_mode:
            runner = SubprocessRunner(
                f"{FORTIVPN_LOCATION} status"
            )
            runner.run()
            if "Connected" in runner.stdout:
                self.forticlient.state = "Connected"
                self._main_window.status_bar_state.setText('Forticlient: Connected')
            else:
                self.forticlient.state = "Disconnected"
                self._main_window.status_bar_state.setText('Forticlient: Disconnected')
            self._check_dns_configuration()
        else:
            logger.info('Monitoring mode disable')

    def _check_dns_configuration(self) -> None:
        """
        This method checks the current DNS configuration.

        It extracts the configured DNS search info from the application settings and
        applies it to the FortiClient instance.
        Then, it pulls in the relevant data from the FortiClient log and
        updates the resolvectl configuration accordingly.

        Note: This method does not return anything.

        Returns:
            None
        """
        self.forticlient.dns_search = self.settings.dns_search
        self.forticlient.extract_log_data()
        self.forticlient.get_or_update_resolvectl()
