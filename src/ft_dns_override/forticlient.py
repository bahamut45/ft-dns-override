import re
from subprocess import CalledProcessError
from typing import Optional, Iterable, Callable, Tuple

from PySide6.QtWidgets import QSystemTrayIcon
from subprocrunner import SubprocessRunner

from ft_dns_override.utils import FORTICLIENT_LOG, logger, qt_icon, send_notification


class Forticlient:
    """

    Class representing a Forticlient.

    Attributes:
        tray_icon (QSystemTrayIcon): The tray icon object associated with the Forticlient instance.
        _state (str): The current state of the Forticlient (e.g., "Connected", "Disconnected").
        _dns_search (str): The DNS search configuration.
        _default_device_name (str): The default device name.
        _default_dns_name_servers (str): The default DNS name servers.
        _vpn_device_name (str): The VPN device name.
        _vpn_dns_name_servers (str): The VPN DNS name servers.

    Methods:
        dns_search (property): Get the DNS search configuration.
        dns_search (setter): Set the DNS search configuration.
        state (property): Get the current state of the Forticlient.
        state (setter): Set the current state of the Forticlient.
        _retrieve_attribute_from_log(file, pattern, callback): Retrieve an attribute from a log file using
        a given pattern and callback function.
        extract_log_data(): Extract relevant data from the Forticlient log file.
        _run_command(cmd): Run a command and return the result and output.
        update_resolvectl(output): Update the resolvectl configuration based on the specified output.
        set_icon_active(): Set the active icon for the tray icon.
        set_icon(): Set the default icon for the tray icon.
        get_or_update_resolvectl(): Get or update the resolvectl configuration based on the current state
        and DNS search configuration.
        resolvectl_modify_dns(): Modify the resolvectl DNS configuration based on the current DNS search configuration.
        _to_log(): Convert the Forticlient object to a string representation for logging purposes.
    """
    def __init__(self, tray_icon: QSystemTrayIcon):
        """
        Initialises Forticlient.

        Args:
            tray_icon (TrayIcon): The tray icon object associated with the Forticlient instance.
        """
        self.tray_icon: QSystemTrayIcon = tray_icon
        self._state: str = "Disconnected"
        self._dns_search: Optional[str] = None
        self._default_device_name: Optional[str] = None
        self._default_dns_name_servers: Optional[str] = None
        self._vpn_device_name: Optional[str] = None
        self._vpn_dns_name_servers: Optional[str] = None

    @property
    def dns_search(self) -> Optional[str]:
        """
        Property that gets the DNS search configuration.

        Returns:
            Optional[str]: DNS search configuration if it exists, None otherwise.
        """
        return self._dns_search

    @dns_search.setter
    def dns_search(self, value: str) -> None:
        """
        Property that sets the DNS search configuration.

        Args:
            value (str): The DNS search configuration to be set.
        """
        self._dns_search = value

    @property
    def state(self) -> str:
        """
        Property that gets the state for the forticlient vpn.

        Returns:
            Optional[str]: Disconnected or Connected.
        """
        return self._state

    @state.setter
    def state(self, value: str) -> None:
        """
        Property that sets the state for the forticlient vpn

        Args:
            value (str): The state to be set.
        """
        self._state = value

    @staticmethod
    def _retrieve_attribute_from_log(file: Iterable, pattern: str, callback: Callable) -> None:
        """
        Retrieves an attribute from a log file using a given pattern and callback function.

        Args:
            file (Iterable): The log file.
            pattern (str): The regex pattern to be found in each line of the log file.
            callback (Callable): Function to call for each matching line.
        """
        for line in file:
            match = re.search(pattern, line)
            if match:
                callback(match)

    def extract_log_data(self) -> None:
        """
        Extracts certain data from the Forticlient log and updates the corresponding attributes.
        """
        patterns_to_attributes = [
            (r"Device name: ([a-z0-9]+)", '_default_device_name'),
            (r"Current DNS list: ([a-z0-9.,]+)", '_default_dns_name_servers'),
            (r"VPN device name: ([a-z0-9]+)", '_vpn_device_name'),
            (r"Set DNS server: ([a-z0-9., ]+)", '_vpn_dns_servers')
        ]

        with open(FORTICLIENT_LOG, 'r') as file:
            for pattern, attribute in patterns_to_attributes:
                self._retrieve_attribute_from_log(
                    file, pattern,
                    lambda m, value=attribute: setattr(
                        self,
                        value,
                        m.group(1)
                    )
                )

    @staticmethod
    def _run_command(cmd: str) -> Tuple[bool, Optional[str]]:
        """
        Executes a command and returns the result and output.

        Args:
            cmd (str): The command to be executed.

        Returns:
            Tuple: success (bool), output (str)
        """
        try:
            runner = SubprocessRunner(cmd)
            runner.run(check=True)
            return True, runner.stdout.strip()
        except CalledProcessError as e:
            logger.error(f"run({cmd}): {e}\n{e.stderr}")
            return False, None

    def update_resolvectl(self, output: str) -> None:
        """
        Updates the resolvectl configuration based on the specified output.

        Args:
            output (str): The current resolvectl configuration output.
        """
        send_notification("Forticlient DNS Override", f"Apply dns search configuration {self.dns_search}")
        logger.info('Change dns search %s to %s', output, self.dns_search)
        self.resolvectl_modify_dns()

    def set_icon_active(self) -> None:
        """
        Sets the tray icon to the active icon.
        """
        self.tray_icon.setIcon(qt_icon('ft-dns-override-active.png'))

    def set_icon(self) -> None:
        """
        Sets the tray icon to the default icon.
        """
        self.tray_icon.setIcon(qt_icon('ft-dns-override-icon.png'))

    def get_or_update_resolvectl(self) -> None:
        """
        Gets or updates the resolvectl configuration based on the current state and DNS search configuration.
        """
        cmd = fr'sudo resolvectl domain {self._default_device_name}'
        result, output = self._run_command(cmd)
        if result:
            output = self._format_output_resolvectl_command(output)
            logger.debug("Get resolvectl configuration: %s", output)
            if output != self.dns_search and self.state == "Connected":
                self.update_resolvectl(output)
            if output == self.dns_search and self.state == "Connected":
                self.set_icon_active()
            if self.state == "Disconnected":
                self.set_icon()

        logger.debug(self._to_log())

    @staticmethod
    def _format_output_resolvectl_command(output):
        if not isinstance(output, str):
            raise TypeError("_format_output_resolvectl_command requires a string as input.")

        try:
            result = output.split(':', 1)[1]
            result = re.findall(r"\S+", result)
            result = " ".join(result)
        except IndexError:
            raise ValueError("Invalid output format. ':' not found in output.")
        except re.error as e:
            raise ValueError(f"Error occured while trying to find all non-whitespace sequences: {e}")

        return result

    def resolvectl_modify_dns(self) -> None:
        """
        Modifies the DNS configuration with the given DNS search domain for the default device.
        """
        cmd = f'sudo resolvectl domain {self._default_device_name} {self.dns_search}'
        logger.debug(cmd)
        result, _ = self._run_command(cmd)
        if result:
            self.set_icon_active()
        else:
            self.set_icon()

    def _to_log(self) -> str:
        """
        Converts the object to a log message representation.

        Returns:
            str: The log message representation of the object.
        """
        return (f'<Forticlient dns_search="{self.dns_search}" '
                f'state="{self.state}" default_device_name="{self._default_device_name}">')
