from pathlib import Path

from PySide6.QtCore import QSettings, QRegularExpression
from PySide6.QtCore import QStandardPaths
from PySide6.QtGui import QRegularExpressionValidator
from PySide6.QtWidgets import QDialog, QLineEdit, QCheckBox, QGroupBox, \
    QFormLayout, QComboBox, QDialogButtonBox

from ft_dns_override.utils import logger, INTERVALS


class SettingsManager:
    """
    This method retrieves a setting value from the application settings.
    If the setting key is found in the application settings, it returns the corresponding value.
    Otherwise, it sets the default value for the setting key in the app settings and returns the default value.
    """

    def __init__(self):
        self.app_settings = QSettings('ft-dns-override', 'Forticlient-Nmcli-Status')
        settings_data_dir = Path('ft-dns-override')
        app_settings_data_dir = QStandardPaths.writableLocation(QStandardPaths.AppConfigLocation) / settings_data_dir
        app_settings_data_dir.mkdir(parents=True, exist_ok=True)
        self._get_setting("default_interval", 10000, "Default settings default_interval: %s ms")
        self._get_setting("monitor_mode", False, "Default settings monitor_mode: %d ms", validate=bool)
        self._get_setting("dns_search", "", "Default settings dns_search : %s ")

    def _get_setting(self, setting_key, default_value, log_message, validate=None):
        """

        Method: _get_setting

        Description:
        This method retrieves a setting value from the application settings.
        If the setting key is found in the application settings,
        it returns the corresponding value. Otherwise, it sets the
        * default value for the setting key in the app settings and returns the default value.

        Parameters:
        - self (object): The class instance.
        - setting_key (str): The key of the setting to retrieve.
        - default_value (any): The default value to set for the setting if it doesn't already exist.
        - log_message (str): The log message to output when setting the default value.
        - validate (function, optional): A validation function to apply to the retrieved setting value.
          Defaults to None.

        Returns:
        - The retrieved or default setting value, or the validated setting value if a validation function is provided.

        """
        if self.app_settings.contains(setting_key):
            setting = self.app_settings.value(setting_key)
            logger.debug('[SettingsManager] Load settings %s: %s', setting_key, setting)
        else:
            self.app_settings.setValue(setting_key, default_value)
            setting = self.app_settings.value(setting_key)
            logger.debug('[SettingsManager] ' + log_message, setting)
        return validate(setting) if validate else setting

    def _set_setting(self, setting_key, value):
        """
        Set a value for a specific setting.

        Parameters:
        - setting_key(str): The key of the setting.
        - value: The value to be set for the setting.

        Example usage:
        ```
        _set_setting('language', 'english')
        ```
        """
        self.app_settings.setValue(setting_key, value)
        logger.debug('[app_settings] Set settings %s: %s', setting_key, value)

    @property
    def default_interval(self):
        return int(self.app_settings.value("default_interval"))

    @property
    def monitor_mode(self):
        return bool(self.app_settings.value("monitor_mode"))

    @property
    def dns_search(self):
        return str(self.app_settings.value("dns_search"))

    @default_interval.setter
    def default_interval(self, value):
        self._set_setting("default_interval", value)

    @monitor_mode.setter
    def monitor_mode(self, value):
        self._set_setting("monitor_mode", value)

    @dns_search.setter
    def dns_search(self, value):
        self._set_setting("dns_search", value)


class SettingsDialog(QDialog):
    """
    A class representing a settings dialog in a GUI application.

    Attributes:
    - settings (Settings): The settings object to be edited in the dialog.

    Methods:
    - __init__(self, settings): Constructs a new SettingsDialog object.
    - save(self): Saves the changes made in the dialog to the settings object.
    """

    def __init__(self, settings, parent=None):
        super().__init__(parent)
        self.parent = parent
        self.settings = settings

        self.layout = QFormLayout()
        self.layout.addWidget(self._settings_dialog())
        self.layout.addWidget(self._settings_buttons())

        self.setLayout(self.layout)

    def _settings_dialog(self):
        settings_groupbox = QGroupBox("")
        form_layout = QFormLayout()
        settings_groupbox.setLayout(form_layout)

        self.form_combo_check_interval = QComboBox(settings_groupbox)
        for interval in INTERVALS:
            self.form_combo_check_interval.addItem(str(f'{interval} Secs'), interval * 1000)
        self.form_combo_check_interval.setCurrentIndex(
            self.form_combo_check_interval.findData(self.settings.default_interval)
        )

        self.form_checkbox_monitoring_mode = QCheckBox(settings_groupbox)
        self.form_checkbox_monitoring_mode.setChecked(self.settings.monitor_mode)

        self.form_line_dns = QLineEdit(settings_groupbox)
        self.form_line_dns.setClearButtonEnabled(True)
        pattern = QRegularExpression(r"[a-z. \-]+")
        validator = QRegularExpressionValidator(pattern)
        self.form_line_dns.setValidator(validator)
        self.form_line_dns.setText(self.settings.dns_search)

        form_layout.addRow('DNS Search:', self.form_line_dns)
        form_layout.addRow('Check Interval:', self.form_combo_check_interval)
        form_layout.addRow('Monitoring Mode:', self.form_checkbox_monitoring_mode)

        return settings_groupbox

    def _settings_buttons(self):
        # noinspection PyUnresolvedReferences
        settings_buttons = QDialogButtonBox(QDialogButtonBox.Save)
        settings_buttons.accepted.connect(self.save)
        return settings_buttons

    def save(self):
        self.settings.default_interval = int(self.form_combo_check_interval.currentData())
        self.settings.monitor_mode = self.form_checkbox_monitoring_mode.isChecked()
        self.settings.dns_search = self.form_line_dns.text()
        if getattr(self.parent, "status_bar"):
            # noinspection PyUnresolvedReferences
            self.parent.status_bar.showMessage('Settings save successfully', 5000)
